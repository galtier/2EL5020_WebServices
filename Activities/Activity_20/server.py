from ariadne import QueryType, graphql_sync, make_executable_schema
from ariadne.explorer import ExplorerGraphiQL
from flask import Flask, jsonify, request

# Data
players_data = [
    {"name": "Marge", "birthYear": 1975, "gender": "F"},
    {"name": "Homer", "birthYear": 1970, "gender": "M"},
    {"name": "Bart", "birthYear": 2005, "gender": "M"},
    {"name": "Lisa", "birthYear": 2007, "gender": "F"},
    {"name": "Ross", "birthYear": 1967, "gender": "M"},
    {"name": "Phoebe", "birthYear": 1970, "gender": "F"},
    {"name": "Monica", "birthYear": 1969, "gender": "F"},
    {"name": "Chandler", "birthYear": 1968, "gender": "M"}
]

teams_data = [
    {"name": "Simpson", "players": ["Marge", "Homer", "Bart", "Lisa"], "captain": "Marge"},
    {"name": "Friends", "players": ["Ross", "Phoebe", "Monica", "Chandler"], "captain": "Ross"}
]

# in the teams_data list, "expends" each player:
def create_rich_teams(teams_data, players_data):
    teams_rich = []

    for team in teams_data:
        new_team = {}
        new_team["name"] = team["name"]
        new_team["captain"] = next((player for player in players_data if player["name"] == team["captain"]), None)
        new_team["players"] = []
        for player_name in team["players"]:
            player = next((player for player in players_data if player["name"] == player_name), None)
            if player:
                new_team["players"].append(player)
        teams_rich.append(new_team)

    return teams_rich



app = Flask("tournament")


# GraphQL Schema
with open('schema.graphql', 'r') as file:
    type_defs = file.read()
# type_defs = """
#     type Query {
#         allTeams: [Team]
#         playerInfo(name: String!): Player
#     }

#     type Player {
#         name: String
#         birthYear: Int
#         gender: String
#     }

#     type Team {
#         name: String
#         players: [Player]
#         captain: Player
# }
# """

query = QueryType()
# resolvers for the queries:
# field name must be the same as in the schema
# resolver name usually starts with "resolve_" followed by field name
@query.field("allTeams")
def resolve_all_teams(_, info):
    return create_rich_teams(teams_data, players_data)

@query.field("playerInfo")
def resolve_player_info(_, info, name: str):
    for player in players_data:
        if player["name"] == name:
            return player
    return "unknown player"

schema = make_executable_schema(type_defs, query)


# On GET request serve the GraphQL explorer ("GraphiQL").
@app.route("/graphql", methods=["GET"])
def graphql_explorer():
    return ExplorerGraphiQL().html(None), 200

@app.route("/api", methods=["GET"])
def graphql_api():
    return type_defs, 200

# GraphQL queries are always sent as POST
@app.route("/graphql", methods=["POST"])
def graphql_server():
    data = request.get_json()
    success, result = graphql_sync(
        schema,
        data)
    status_code = 200 if success else 400
    return jsonify(result), status_code


app.run(port=5000)
