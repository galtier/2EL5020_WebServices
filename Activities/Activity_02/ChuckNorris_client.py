import requests

url = "https://api.chucknorris.io/jokes/random"

response = requests.get(url)
    
if response.status_code == 200:
    print(response.text)
else:
    print("Error:", response.status_code)
