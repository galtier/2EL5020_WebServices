import requests
import json

base_url = "http://virginiegaltier.eu.pythonanywhere.com/"

response = requests.delete(base_url + "/members")
assert response.status_code == 204

response = requests.get(base_url + "/members")
assert response.status_code == 200
assert response.text == "[]"

response = requests.get(base_url + "/members/FZ9")
assert response.status_code == 404
assert response.text == "Unknown license number"

john = {"name": "John", "birthYear": 2010, "gender": "M"}
headers = {"Content-Type": 'application/json'}
response = requests.post(url=base_url + "/members", headers=headers, json=john)
assert response.status_code == 201
assert "Location" in response.headers
assert response.headers["Location"] == "/members/MJ1"

response = requests.get(base_url + "/members")
assert response.status_code == 200
members = json.loads(response.text)
assert len(members) == 1
john_expected = {"name": "John", "birthYear": 2010, "gender": "M", "licenseNumber": "MJ1"}
assert members[0] == john_expected

mary = {"name": "Mary", "birthYear": 2015, "gender": "F"}
headers = {"Content-Type": 'application/json'}
response = requests.post(url=base_url + "/members", headers=headers, json=mary)
assert response.status_code == 201
assert "Location" in response.headers
assert response.headers["Location"] == "/members/FM2"

response = requests.get(base_url + "/members")
assert response.status_code == 200
john_expected = {"name": "John", "birthYear": 2010, "gender": "M", "licenseNumber": "MJ1"}
mary_expected = {"name": "Mary", "birthYear": 2015, "gender": "F", "licenseNumber": "FM2"}
john_found = False
mary_found = False
other_found = False
for element in json.loads(response.text):
    if element == john_expected:
        john_found = True
    elif element == mary_expected:
        mary_found = True
    else:
        other_found = True
assert john_found
assert mary_found
assert not other_found

response = requests.get(base_url + "/membersInRange?ageMin=20&ageMax=25")
assert response.status_code == 400
assert response.text == "Bad Request: Missing 'minAge' or 'maxAge' parameter"

response = requests.get(base_url + "/membersInRange?minAge=10&maxAge=25")
assert response.status_code == 200
members = json.loads(response.text)
assert len(members) == 1
john_expected = {"name": "John", "birthYear": 2010, "gender": "M", "licenseNumber": "MJ1"}
assert members[0] == john_expected

response = requests.get(base_url + "/members/FM2")
assert response.status_code == 200
mary_actual = json.loads(response.text)
mary_expected = {"name": "Mary", "birthYear": 2015, "gender": "F", "licenseNumber": "FM2"}
assert mary_actual == mary_expected

response = requests.delete(base_url + "/members/MJ1")
assert response.status_code == 204

response = requests.get(base_url + "/members")
assert response.status_code == 200
mary_expected = {"name": "Mary", "birthYear": 2015, "gender": "F", "licenseNumber": "FM2"}
mary_found = False
other_found = False
for element in json.loads(response.text):
    if element == mary_expected:
        mary_found = True
    else:
        other_found = True
assert mary_found
assert not other_found

response = requests.delete(base_url + "/members/MJ1")
assert response.status_code == 404
assert response.text == "Unknown license number"

marylou = {"name": "Mary-Lou"}
headers = {"Content-Type": 'application/json'}
response = requests.put(url=base_url + "/members/FM2", headers=headers, json=marylou)
assert response.status_code == 204

response = requests.get(base_url + "/members/FM2")
assert response.status_code == 200
marylou_actual = json.loads(response.text)
marylou_expected = {"name": "Mary-Lou", "birthYear": 2015, "gender": "F", "licenseNumber": "FM2"}
assert marylou_actual == marylou_expected

response = requests.delete(base_url + "/members")
assert response.status_code == 204

response = requests.get(base_url + "/members")
assert response.status_code == 200
assert response.text == "[]"

