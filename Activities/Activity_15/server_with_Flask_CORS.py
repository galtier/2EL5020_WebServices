from flask import Flask
from flask import request, Response
import json
from flask_cors import CORS

lastId = 0
members = []
"""
alice = {"licenseNumber": "FA0", "name": "Alice", "birthYear": 2000, "gender": "F"}
members.append(alice)
bob = {"licenseNumber": "MB1", "name": "Bob", "birthYear": 2001, "gender": "M"}
members.append(bob)
"""

# creates a Flask application object (run at the last line of this code)
#-----------------------------------------------------------------------
app = Flask("association")
CORS(app)

# create routes
#--------------

# route() is a Flask method (app is a Flask object) used to "decorate" ("@")
# a function and register it with the given URL and HTTP method ("GET" by default)

@app.route('/')
def welcome():
    # The return value is a string.
    # Flask converts it into a response object with the string as response body,
    # a 200 OK status code and a text/html mimetype.
    return "Hello World!"

# Often people writing a Flask app use the same word for the route and the function,
# but this is not mandatory.
@app.route('/members')
def get_the_list_of_all_members():
    global members
    return str(json.dumps(members)) # pythonanywhere requests specific return types

# demonstrates the use of a variable in a Flask route
# demonstrates the use of different HTTP methods with the same url
# demonstrates how to return a HTTP response code that is not 200 OK
@app.route('/members/<license_number>', methods=['GET', 'DELETE'])
def member(license_number):
    global members
    target_member = None
    for member in members:
        if member["licenseNumber"] == license_number:
            target_member = member
            if request.method == 'GET':
                return(target_member)
            if request.method == 'DELETE':
                members.remove(member)
                return("member removed", 204)                
    return("Unknown license number", 404)

# Demonstrates the usage of url arguments: /membersInRange?minAge=18&maxAge=40
@app.route('/membersInRange')
def get_members_in_age_range():
    global members
    query_params_dict = request.args
    if 'minAge' in query_params_dict and 'maxAge' in query_params_dict:
        min_age = int(query_params_dict['minAge'])
        max_age = int(query_params_dict['maxAge'])
        members_in_range = []
        for member in members:
            age = 2024 - member["birthYear"]
            if min_age <= age <= max_age:
                members_in_range.append(member)
        return str(json.dumps(members_in_range))
    else:
        return("Bad Request: Missing 'minAge' or 'maxAge' parameter", 400)

@app.route('/members', methods=['DELETE'])
def reset():
    global members
    global lastId
    members = []
    lastId = 0
    return Response(status=204)

# demonstrates access to the request body
# demonstrates use of the Response class
@app.route('/members', methods=['POST'])
def create():
    global members
    global lastId
    json_member = request.get_json()
    new_member = json_member
    lastId = lastId + 1
    licenseNumber = json_member["gender"] + json_member["name"][0] + str(lastId)
    new_member["licenseNumber"] = licenseNumber
    members.append(new_member)
    response = Response(response="new member added", status=201)
    response.headers['Location'] = "/members/" + licenseNumber
    return response

@app.route('/members/<license_number>', methods=['PUT'])
def update(license_number):
    global members
    target_member = None
    for member in members:
        if member["licenseNumber"] == license_number:
            target_member = member
            json_member = request.get_json()
            member["name"] =json_member["name"]
            return("member updated", 204)       
    return("Unknown license number", 404)

# remove on pythonanywhere:
app.run()
