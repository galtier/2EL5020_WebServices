from flask import Flask
from flask import request, Response
import json
from flask_cors import CORS
from apiflask import APIFlask, Schema
from apiflask.fields import Integer, String
from apiflask.validators import OneOf

lastId = 0
members = []
"""
alice = {"licenseNumber": "FA0", "name": "Alice", "birthYear": 2000, "gender": "F"}
members.append(alice)
bob = {"licenseNumber": "MB1", "name": "Bob", "birthYear": 2001, "gender": "M"}
members.append(bob)
"""

# creates a Flask application object (run at the last line of this code)
#-----------------------------------------------------------------------
app = APIFlask("association", title='Membership management', version='1')
app.config['SPEC_FORMAT'] = 'yaml'
app.config['LOCAL_SPEC_PATH'] = 'openapi.yaml'
app.config['OPENAPI_VERSION'] = '3.0.3'
#app.openapi_version = '3.0.3'
app.config['DESCRIPTION'] = 'Tool to manage the members of an association.'
app.config['SERVERS'] = [ { 'url': 'http://localhost:5000'}]

CORS(app)

class Person(Schema):
    name = String()
    birthYear = Integer()
    gender = String(validate=OneOf(['M', 'F']))

class Member(Person):
    licenseNumber = String()

# create routes
#--------------

@app.route('/members')
@app.doc(summary='List all members', description='Returns a list of all members, with all their information.')
@app.output(Member(many=True), status_code=200, description='Successful operation')
def get_the_list_of_all_members():
    global members
    return str(json.dumps(members)) # pythonanywhere requests specific return types

@app.route('/members/<license_number>', methods=['GET', 'DELETE'])
@app.doc(summary='Find member by license number', description='Returns all information about the member whose license number is given.')
@app.output(Member, status_code=200, description='Successful operation')
def member(license_number):
    global members
    target_member = None
    for member in members:
        if member["licenseNumber"] == license_number:
            target_member = member
            if request.method == 'GET':
                return(target_member)
            if request.method == 'DELETE':
                members.remove(member)
                return("member removed", 204)                
    return("Unknown license number", 404)

# Demonstrates the usage of url arguments: /membersInRange?minAge=18&maxAge=40
@app.route('/membersInRange')
@app.doc(summary='List members in the provided age range', description='Returns a list of all members older than minAge and younger than maxAge, provide all their information.')
@app.output(Member(many=True), status_code=200, description='Successful operation')
def get_members_in_age_range():
    global members
    query_params_dict = request.args
    if 'minAge' in query_params_dict and 'maxAge' in query_params_dict:
        min_age = int(query_params_dict['minAge'])
        max_age = int(query_params_dict['maxAge'])
        members_in_range = []
        for member in members:
            age = 2024 - member["birthYear"]
            if min_age <= age <= max_age:
                members_in_range.append(member)
        return str(json.dumps(members_in_range))
    else:
        return("Bad Request: Missing 'minAge' or 'maxAge' parameter", 400)

@app.route('/members', methods=['DELETE'])
@app.doc(summary='Delete all members', description='Delete all members and reset the counter used to generated license numbers to 0.')
@app.output({}, status_code=204, description='Successful removal')
def reset():
    global members
    global lastId
    members = []
    lastId = 0
    return Response(status=204)

# demonstrates access to the request body
# demonstrates use of the Response class
@app.route('/members', methods=['POST'])
@app.doc(summary='Add a new member', description='Add a person (as described in the body) to the list of members, a new license number is issued, attached to the new member, and returned in the Location header.')
@app.input(Person) #, description="Information to create the new member")
@app.output({}, status_code=201, description='Successful creation', headers={'Location': "url of new member"})
def create():
    global members
    global lastId
    json_member = request.get_json()
    new_member = json_member
    lastId = lastId + 1
    licenseNumber = json_member["gender"] + json_member["name"][0] + str(lastId)
    new_member["licenseNumber"] = licenseNumber
    members.append(new_member)
    response = Response(response="new member added", status=201)
    response.headers['Location'] = "/members/" + licenseNumber
    return response

class LicenseNumber(Schema):
    license_number = String(required=True)

@app.route('/members/<license_number>', methods=['PUT'])
@app.doc(summary='Find member by license number', description='Returns all information about the member whose license number is given.')
@app.input(LicenseNumber, location='path') #, description='License number of the member who has changed name.')
@app.output(Member, status_code=200, description='Successful operation')
@app.output({}, status_code=404, description='Unknown license number')
def update(license_number):
    global members
    target_member = None
    for member in members:
        if member["licenseNumber"] == license_number:
            target_member = member
            json_member = request.get_json()
            member["name"] =json_member["name"]
            return("member updated", 204)       
    return("Unknown license number", 404)

# remove on pythonanywhere:
app.run()