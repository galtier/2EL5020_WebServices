import connexion
import six
from flask import jsonify, Response

from swagger_server.models.new_question import NewQuestion  # noqa: E501
from swagger_server.models.question import Question  # noqa: E501
from swagger_server import util

categories = ["syntax", "history", "concept", "acronym"]
questions = []

def add_new_question(body): 
    """Add a new question

    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Question.from_dict(connexion.request.get_json())  # noqa: E501
        new_id = str(len(questions) + 1)  # Assign a new ID
        body.question_id = new_id
        questions.append(body)
        response = Response("Question added successfully", status=201)
        response.headers['Location'] = '/questions/' + new_id
        return response
    else:
        return "Invalid JSON", 400


def get_all_categories():
    """Get available question categories

    :rtype: List[str]
    """
    return jsonify(categories)


def get_all_questions():
    """Get all questions

    :rtype: List[Question]
    """
    return jsonify(questions)


def get_question_by_id(question_id):
    """Get a question by ID

    :param id: The ID of the question to retrieve
    :type id: int

    :rtype: Question
    """
    for question in questions:
        if question.question_id == str(question_id):
            return jsonify(question)
    return "Question not found", 404

def get_questions_by_category(category): 
    """Get questions by category

    :param category: The category of questions to retrieve
    :type category: str

    :rtype: List[Question]
    """
    filtered_questions = [question for question in questions if question.category == category]
    return jsonify(filtered_questions)