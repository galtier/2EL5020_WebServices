from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

print("Trivial Pursuit")

# create an instance of the API class
configuration = swagger_client.Configuration()
configuration.host = 'http://localhost:8080'
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

# Get all questions
try:
    api_response = api_instance.get_all_questions()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_questions: %s\n" % e)

# Get available question categories
try:
    api_response = api_instance.get_all_categories()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_categories: %s\n" % e)

# Add 2 new questions
body = swagger_client.NewQuestion()
body.category = "history"
body.question = "When was the Representational State Transfer (REST) architectural style proposed, by whom, and in which context?"
body.correct_answer = "Proposed in 2000 by Roy Fielding as part of his doctoral dissertation on network-based software architectures"
body.incorrect_options = [ "Proposed in 1996 by Tim Berners-Lee as a communication standard for the World Wide Web", "Proposed in 2005 by Mark Nottingham as a solution for improving web scalability", "Proposed in 2001 by Larry Page and Sergey Brin as a protocol for interconnecting web services" ]
try:
    api_instance.add_new_question(body)
except ApiException as e:
    print("Exception when calling DefaultApi->add_new_question: %s\n" % e)

body = swagger_client.NewQuestion()
body.category = "syntax"
body.question = "What is the correct syntax for representing key-value pairs in JSON and YAML?"
body.correct_answer = "JSON: name: \"John\", YAML: name = \"John\""
body.incorrect_options = [ "JSON: name = \"John\", YAML: name: \"John\"", "JSON: {name: \"John\"}, YAML: name: \"John\"", "JSON: {name: \"John\"}, YAML: name = \"John\""]
try:
    api_instance.add_new_question(body)
except ApiException as e:
    print("Exception when calling DefaultApi->add_new_question: %s\n" % e)

# Get all questions
try:
    api_response = api_instance.get_all_questions()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_questions: %s\n" % e)

# Get a question by ID
question_id = 56 # int | The ID of the question to retrieve
try:
    api_response = api_instance.get_question_by_id(question_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_question_by_id: %s\n" % e)

question_id = 1 # int | The ID of the question to retrieve
try:
    api_response = api_instance.get_question_by_id(question_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_question_by_id: %s\n" % e)

# Get questions by category
category = 'syntax' # str | The category of questions to retrieve
try:
    api_response = api_instance.get_questions_by_category(category)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_questions_by_category: %s\n" % e)
