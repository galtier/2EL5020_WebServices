from flask import Flask
from flask import request
import json

members = []
alice = {"licenseNumber": "A07", "name": "Alice", "birthYear": 2000, "gender": "F"}
members.append(alice)
bob = {"licenseNumber": "C11", "name": "Bob", "birthYear": 2001, "gender": "M"}
members.append(bob)

# creates a Flask application object (run at the last line of this code)
#-----------------------------------------------------------------------
app = Flask("association")

# create routes
#--------------

# route() is a Flask method (app is a Flask object) used to "decorate" ("@")
# a function and register it with the given URL and HTTP method ("GET" by default)

@app.route('/')
def welcome():
    # The return value is a string.
    # Flask converts it into a response object with the string as response body, 
    # a 200 OK status code and a text/html mimetype.
    return "Hello World!"

# Often people writing a Flask app use the same word for the route and the function,
# but this is not mandatory.
@app.route('/members')
def get_the_list_of_all_members():
    global members
    return members

# demonstrates the use of a variable in a Flask route
@app.route('/members/<license_number>')
def member(license_number):
    global members
    target_member = None
    found = False
    for member in members:
        if member["licenseNumber"] == license_number:
            target_member = member
            found = True
            break
    if found:
        return(target_member)
    else:
        return("Unknown license number", 404)    

# Demonstrates the usage of url arguments: /membersInRange?minAge=18&maxAge=40
@app.route('/membersInRange')
def get_members_in_age_range():
    global members
    query_params_dict = request.args
    if 'minAge' in query_params_dict and 'maxAge' in query_params_dict:
        min_age = int(query_params_dict['minAge'])
        max_age = int(query_params_dict['maxAge'])
        members_in_range = []
        for member in members:
            age = 2024 - member["birthYear"]
            if min_age <= age <= max_age:
                members_in_range.append(member)
        return members_in_range 
    else:
        return("Bad Request: Missing 'minAge' or 'maxAge' parameter", 400)
app.run()
