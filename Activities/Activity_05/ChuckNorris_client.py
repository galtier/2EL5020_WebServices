import requests
import json

url = "https://api.chucknorris.io/jokes/random"

response = requests.get(url)
    
if response.status_code == 200:
    json_response = response.text
    dict_response = json.loads(json_response)
    print(dict_response["value"])
else:
    print("Error:", response.status_code)
