import json

# JSON string representing the family
family_json = '''
{
  "family": {
    "parents": [
      {
        "name": "Alice",
        "birth_year": 1950
      },
      {
        "name": "Bob",
        "birth_year": 1950
      }
    ],
    "children": [
      {
        "name": "Charly",
        "birth_year": 1975,
        "children": [
          {
            "name": "Fred",
            "birth_year": 2005
          },
          {
            "name": "Ginny",
            "birth_year": 2009
          }
        ]
      },
      {
        "name": "Daniel",
        "birth_year": 1978
      },
      {
        "name": "Elisabeth",
        "birth_year": 1981,
        "children": [
          {
            "name": "Henry",
            "birth_year": 2006
          }
        ]
      }
    ]
  }
}
'''

# Parse JSON string into Python dictionary
family_data = json.loads(family_json)

# Get the children of Alice and Bob:
children = family_data['family']['children']

# Find Charly
for child in children:
    # Each child has a name and a birth year and might have children
    if child['name'] == 'Charly':
        children_of_charly = child['children']
        # Print Charly's children
        print("Charly's children:")
        for child_of_charly in children_of_charly:
            print(child_of_charly['name'])
        # Charly was found, no need to examin the other children
        break