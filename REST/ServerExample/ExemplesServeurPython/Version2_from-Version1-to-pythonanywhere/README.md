# Version 2: from Version 1 to PythonAnywhere

Create an account on [https://eu.pythonanywhere.com](https://eu.pythonanywhere.com)

- Open Web tab
- Add a new web app
- keep the free domain name, Next
- select Flask, Python 3.10
- path: /home/yourpythonanywhereusername/v2/membership.py, Next
- go to v2 directory, edit the membership.py file
- copy the local membership.py file
- remove the last line: `app.run()`
- Save and Run the file
- Test:
```
curl -X DELETE http://yourpythonanywhereusername.eu.pythonanywhere.com/reset

curl http://yourpythonanywhereusername.eu.pythonanywhere.com/members

curl -X POST http://yourpythonanywhereusername.eu.pythonanywhere.com/newMember \
    -H 'Content-Type: application/json' \
    -d '{
  "name": "Alice",
  "birthYear": 2001,
  "gender": "F",
  "plan": "regular"
}'

curl -X POST http://yourpythonanywhereusername.eu.pythonanywhere.com/newMember \
    -H 'Content-Type: application/json' \
    -d '{
  "name": "Bob",
  "birthYear": 2017,
  "gender": "M",
  "plan": "free"
}'

curl 'http://yourpythonanywhereusername.eu.pythonanywhere.com/ageRange?ageMin=10&ageMax=40'

curl http://yourpythonanywhereusername.eu.pythonanywhere.com/member/1

curl -X DELETE http://yourpythonanywhereusername.eu.pythonanywhere.com/member/1

curl http://yourpythonanywhereusername.eu.pythonanywhere.com/members
```
