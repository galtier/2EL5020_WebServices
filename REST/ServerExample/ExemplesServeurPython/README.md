# Mise en route rapide pour un petit serveur

Cet espace est destiné aux élèves qui ont besoin de mettre en place dans le cadre d'un projet un petit serveur web, c'est-à-dire une application tournant en permanence à l'écoute de requêtes HTTP qui lui seraient adressées, capable de les traiter, et de renvoyer des réponses HTTP. Le code sera en Python.

Ces ressources ne constituent pas un cours et n'ont que la visée pragmatique de mettre rapidement en place un serveur minimaliste. L'exemple choisi est celui d'un serveur qui gérerait les adhésions d'une association : on peut interroger le serveur pour connaître la liste des membres, pour connaître les informations détaillées sur un membre (nom, année de naissance, genre, et type d'abonnement souscrit), supprimer un membre, ajouter un nouveau membre, ou connaître la liste des membres dans un intervalle d'âge donné. Il y a néanmoins une progression dans les ressources :

## Version 1 : le serveur tourne en local, et les données sont stockées dans un dictionnaire

Cette première version stocke les membres de l'association dans un dictionnaire appelé `members`. La clé correspond à l'identifiant d'un membre (un entier, que l'on incrémente à chaque création d'un nouvel adhérent, la variable globale `lastId` permettant de se souvenir du prochain identifiant à attribuer). La valeur correspond à un dictionnaire comportant lui-même 4 clés : `name`, `birthYear`, `gender`, et `plan`.

Le serveur utilise [Flask](https://flask.palletsprojects.com/en/2.2.x/) et attend par défaut les requêtes sur l'adresse 127.0.0.1:5000 (accessible uniquement depuis votre machine).

Pour tester le serveur on peut utiliser l'utilitaire [`curl`](https://curl.se/) ou bien installer [l'extension RESTer pour Firefox](https://addons.mozilla.org/en-US/firefox/addon/rester/) ou [pour Chrome](https://chrome.google.com/webstore/detail/rester/eejfoncpjfgmeleakejdcanedmefagga?hl=en).

Et ensuite ?
- Si vous préférez utiliser une base de données au lieu de stocker l'état applicatif dans un dictionnaire, passez à la version 4.
- Si vous préférez que le serveur s'exécute sur le cloud et puisse être accédé de n'importe où, passez à la version 2.
- Si vous préférez suivre les bonnes pratiques de génie logiciel et adopter une approche "design-first", passez à la version 3.
- La version 5 réunit les versions 2 et 4 : l'application s'exécute sur le cloud et les données sont stockées dans une base de données hébergée elle aussi sur le cloud.

## Version 2 : passage sur pythonanywhere

[Pythonanywhere](https://www.pythonanywhere.com/) est un service d'hébergement d'applications web basées sur Python. Avec l'abonnement gratuit "beginner" vous pouvez faire tourner le code développé dans la version 1 sur le cloud (au lieu de sur votre machine). Votre service devient alors accessible à une adresse publique.

## Version 3 : génération du squelette du code serveur depuis une spécification OpenAPI

[OpenAPI](https://www.openapis.org/) permet de documenter une API REST. A l'aide de [l'éditeur Swagger](https://editor.swagger.io/), il est possible de générer le squelette du code du serveur à partir d'un document OpenAPI. Le contrat entre le client et le serveur est clair, et le développeur peut se concentrer sur le coeur des méthodes (code métier) plutôt que sur la définition des routes etc.

## Version 4 : stockage des données dans une base locale gérée par MySQL

Dans cette partie vous utiliserez une image [Docker](https://www.docker.com/) pour exécuter localement un serveur [MySQL](https://www.mysql.com/) sans avoir à installer MySQL et ses dépendances sur votre machine (vous aurez besoin d'installer Docker néanmoins). Puis vous vous connecterez au serveur pour créer une base de données "association".

Ensuite vous modifierez le code applicatif de la version 1 pour créer une table "Members" dans la base "association", et vous modifierez chaque méthode pour remplacer les lectures et écritures dans le dictionnaire `members` par des requêtes SQL sur la table "Members".

## Version 5 : base de données et application web sur pythonanywhere

Pythonanywhere vous offre aussi la possibilité de créer une base de données sur un serveur MySQL hébergé. Dans cette dernière partie vous apportez les modifications nécessaires pour passer de la version 4 à une version hébergée sur le cloud.

