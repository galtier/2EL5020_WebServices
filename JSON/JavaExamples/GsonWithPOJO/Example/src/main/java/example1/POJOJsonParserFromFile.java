package example1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;

public class POJOJsonParserFromFile {

	public static void main(String[] arg) throws IOException {
		// The JSON string to parse:
		String jsonString = new String(Files.readAllBytes(Paths.get("charly.json")));
		// The Gson instance used to parse the string:
		Gson gsonParser = new Gson();
		
		// Create a Java object from the string:
		Student charly = gsonParser.fromJson(jsonString, Student.class);

		System.out.println(charly);
	}
}
