package exercise;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.google.gson.Gson;

public class ContactParser {

	public static void main(String[] arg) throws IOException {
		String personName = "John Doe";
		try {
			String jsonString = new String(Files.readAllBytes(Paths.get("addressBook.json")));
			Gson gson = new Gson();
			AddressBook addressBook = gson.fromJson(jsonString, AddressBook.class);
			List<Contact> contacts = addressBook.getContacts();
			for (Contact contact : contacts) {
				if (contact.getName().equals(personName)) {
					List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
					System.out.println("numbers for " + personName + ": ");
					for (PhoneNumber phoneNumber : phoneNumbers) {
						System.out.println(phoneNumber);
					}
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
