package exercise;

class PhoneNumber {
	private String number;
	private String type;

	@Override
	public String toString() {
		return type + ": " + number;
	}
}