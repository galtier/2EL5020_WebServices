package exercise2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ContactParser {

	public static void main(String[] arg) throws IOException {
		// The JSON string to parse:
		String jsonString = new String(Files.readAllBytes(Paths.get("addressBook.json")));
		// The Gson instance used to parse the string:
		Gson gsonParser = new Gson();

		// Create a JSON tree from the string:
		JsonObject addressBook = gsonParser.fromJson(jsonString, JsonObject.class);

		// print number of phone numbers for searched contacts
		System.out.println(getnbPhoneNumbers(addressBook, "John Doe"));
		System.out.println(getnbPhoneNumbers(addressBook, "Tartempion Untel"));
		System.out.println(getnbPhoneNumbers(addressBook, "Foo"));
	}

	static int getnbPhoneNumbers(JsonObject addressBook, String name) {
		JsonArray peopleArray = addressBook.getAsJsonArray("people");
		for (JsonElement person : peopleArray) {
			String pName = person.getAsJsonObject().getAsJsonPrimitive("name").getAsString();
			if (pName.equals(name)) {
				JsonArray phoneNumbers = person.getAsJsonObject().getAsJsonArray("phoneNumbers");
				return phoneNumbers.size();
			}
		}
		return 0;
	}

}
