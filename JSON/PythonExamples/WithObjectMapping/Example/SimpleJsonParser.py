import json
import jsonpickle

# the JSON string to parse:
jsonString = """{
   "name":"Charly",
   "age":20,
   "clubs":[
      "reading",
      "cooking"
   ],
   "grades":{
      "maths":17,
      "computer science":16
   }
}"""

# create a dictionary from the JSON string:
charly_dict = json.loads(jsonString)
# or create a dictionary from the JSON file:
f = open('charly.json')
charly_dict = json.load(f)
f.close()

# add fields for jsonpickle:
charly_dict["py/object"] = "Student.Student"
# create a string from the dictionary with double quotes instead of single quotes:
serialized_charly = str(charly_dict).replace("'",'"')

# and decode:
charly = jsonpickle.decode(serialized_charly)
print(type(charly))
print(charly)
