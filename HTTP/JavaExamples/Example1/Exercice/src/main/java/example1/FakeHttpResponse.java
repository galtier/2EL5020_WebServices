package example1;

import java.net.URI;
import java.net.http.HttpClient.Version;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

import javax.net.ssl.SSLSession;

public class FakeHttpResponse<T> implements HttpResponse<T> {

	private int statusCode;
	private HttpRequest request;
	private Optional<HttpResponse<T>> previousResponse;
	private HttpHeaders headers;
	private T body;
	private Optional<SSLSession> sslSession;
	private URI uri;
	private Version version;

	public FakeHttpResponse(int statusCode, HttpRequest request, Optional<HttpResponse<T>> previousResponse,
			HttpHeaders headers, T body, Optional<SSLSession> sslSession, URI uri, Version version) {
		super();
		this.statusCode = statusCode;
		this.request = request;
		this.previousResponse = previousResponse;
		this.headers = headers;
		this.body = body;
		this.sslSession = sslSession;
		this.uri = uri;
		this.version = version;
	}

	public int statusCode() {
		return statusCode;
	}

	public HttpRequest request() {
		return request;
	}

	public Optional<HttpResponse<T>> previousResponse() {
		return previousResponse;
	}

	public HttpHeaders headers() {
		return headers;
	}

	public T body() {
		return body;
	}

	public Optional<SSLSession> sslSession() {
		return sslSession;
	}

	public URI uri() {
		return uri;
	}

	public Version version() {
		return version;
	}

}
