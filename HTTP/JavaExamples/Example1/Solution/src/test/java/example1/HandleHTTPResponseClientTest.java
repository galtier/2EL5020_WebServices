package example1;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.http.HttpResponse;

import org.junit.Test;

public class HandleHTTPResponseClientTest {

	public HandleHTTPResponseClientTest() {	
	}
	
	@Test
	public void testDisplayHttpResponse() {
		HttpResponse<String> response = HandleHTTPResponseClient.httpResponseWithoutBody();
		String consolePrint = null;
		PrintStream originalOut = System.out;
		try {
		ByteArrayOutputStream os = new ByteArrayOutputStream(100);
		PrintStream capture = new PrintStream(os);
		System.setOut(capture);
		HandleHTTPResponseClient.displayHttpResponse(response);
		capture.flush();
		consolePrint = os.toString();
		} finally {
			System.setOut(originalOut);
		}
		String expected = "STATUS:\n"
				+ "-------\n"
				+ "200\n"
				+ "\n"
				+ "EMPTY RESPONSE BODY\n"
				+ "";
		assertEquals(expected, consolePrint);
		
		
		response = HandleHTTPResponseClient.httpResponseWithBody();
		consolePrint = null;
		try {
		ByteArrayOutputStream os = new ByteArrayOutputStream(100);
		PrintStream capture = new PrintStream(os);
		System.setOut(capture);
		HandleHTTPResponseClient.displayHttpResponse(response);
		capture.flush();
		consolePrint = os.toString();
		} finally {
			System.setOut(originalOut);
		}
		expected = "STATUS:\n"
				+ "-------\n"
				+ "200\n"
				+ "\n"
				+ "BODY:\n"
				+ "-----\n"
				+ "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
				+ "<html>\n"
				+ "<head>\n"
				+ "<title>Test HTML File</title>\n"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "\n"
				+ "<p>This is a very simple HTML file.</p>\n"
				+ "\n"
				+ "</body>\n"
				+ "</html>\n\n";
		assertEquals(expected, consolePrint);
		
	}

}
