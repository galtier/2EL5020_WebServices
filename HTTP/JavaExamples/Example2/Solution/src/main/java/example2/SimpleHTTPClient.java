package example2;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class SimpleHTTPClient {
	public static void main(String[] args) {
		// Build the client
		// Version 1: with the default settings (GET method, HHTP/2, default proxy...):
		// HttpClient client = HttpClient.newHttpClient();
		// Version 2: with a custom builder, for instance:
		HttpClient client = HttpClient.newBuilder().version(Version.HTTP_1_1).build();

		// URI
		URI uri = URI.create("http://www.brainjar.com/java/host/test.html");

		// TP request
		// Version 1: for simple requests:
		// HttpRequest request = HttpRequest.newBuilder(uri).build();
		// Version 2: when you need to configure some parameters:
		HttpRequest request = HttpRequest.newBuilder().uri(uri).GET()
				.header("Accept-Language", "fr-FR, fr;q=0.9, *;q=0.5").build();
		try {
			// Receive the response body as a string:
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			// Display response body
			System.out.println(response.body());
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}
}
