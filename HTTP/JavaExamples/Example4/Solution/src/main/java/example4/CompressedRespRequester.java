package example4;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class CompressedRespRequester {
	public static void main(String[] args) {
		HttpClient client = HttpClient.newHttpClient();
		URI uri = URI.create("http://www.columbia.edu/~fdc/sample.html");
		HttpRequest request = HttpRequest.newBuilder()
				.uri(uri)
				.header("Accept-Encoding", "gzip")
				.build();
		try {
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			System.out.println(response.body());
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}
}
