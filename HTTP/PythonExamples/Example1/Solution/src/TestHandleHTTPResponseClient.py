import requests
from HandleHTTPResponseClient import display_http_response

URL = "http://www.brainjar.com/java/host/test.html"

response = requests.get(url=URL)

display_http_response(response)

response = requests.head(url=URL)

display_http_response(response)
