import requests
def display_http_response(r):
    print("STATUS:")
    print("-------")
    print(r.status_code)
    print("")
    if r.text == "":
        print("EMPTY RESPONSE BODY")
    else:
        print(r.text)
