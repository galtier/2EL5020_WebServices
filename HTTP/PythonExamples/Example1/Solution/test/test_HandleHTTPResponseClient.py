from src.HandleHTTPResponseClient import display_http_response
import requests
from io import StringIO
import sys


def test_display_http_response_with_body():
    # endpoint
    URL = "http://www.brainjar.com/java/host/test.html"

    # sending HTTP requests and saving the responses
    response1 = requests.get(url=URL)

    # call the display method and save output
    capturedOutputResponse1 = StringIO()  # Create StringIO object
    sys.stdout = capturedOutputResponse1  # and redirect stdout.
    display_http_response(response1)  # Call unchanged function.

    # Reset redirect, now works as before.
    sys.stdout = sys.__stdout__

    expectedResponse1 = """STATUS:
-------
200

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\r
<html>\r
<head>\r
<title>Test HTML File</title>\r
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />\r
</head>\r
<body>\r
\r
<p>This is a very simple HTML file.</p>\r
\r
</body>\r
</html>\r

"""
    assert expectedResponse1 == capturedOutputResponse1.getvalue()

def test_display_http_response_without_body():
    # endpoint
    URL = "http://www.brainjar.com/java/host/test.html"

    # sending HTTP requests and saving the responses
    response2 = requests.head(url=URL)

    # call the display method and save output
    capturedOutputResponse2 = StringIO()  # Create StringIO object
    sys.stdout = capturedOutputResponse2  # and redirect stdout.
    display_http_response(response2)  # Call unchanged function.

    # Reset redirect, now works as before.
    sys.stdout = sys.__stdout__

    expectedResponse2 = """STATUS:
-------
200

EMPTY RESPONSE BODY
"""
    assert expectedResponse2 == capturedOutputResponse2.getvalue()
