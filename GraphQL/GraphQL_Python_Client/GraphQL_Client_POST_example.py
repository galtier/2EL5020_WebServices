import requests

URL = "https://countries.trevorblades.com"

body = """
{
  country(code: "FR"){
    name
    capital
    continent {
      name
    }
    currency
  }
}
"""

response = requests.post(url=URL, json={"query": body})

print(response.text)

