import requests

URL = "https://countries.trevorblades.com"

query = """
{
  country(code: "FR"){
    name
    capital
    continent {
      name
    }
    currency
  }
}
"""

response = requests.get(url=URL, params={'query': query})

#print(response.url)

print(response.text)

