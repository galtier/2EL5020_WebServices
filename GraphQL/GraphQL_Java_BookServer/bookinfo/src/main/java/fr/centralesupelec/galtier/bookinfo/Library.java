package fr.centralesupelec.galtier.bookinfo;

import java.util.Arrays;
import java.util.List;

public class Library {

	private static List<Author> authors = Arrays.asList(
			new Author("author-1", "Joanne", "Rowling"),
			new Author("author-2", "Herman", "Melville"), 
			new Author("author-3", "Anne", "Rice"));
	private static List<Book> books = Arrays.asList(
			new Book("book-1", "Harry Potter and the Philosopher's Stone", 223, authors.get(0)),
			new Book("book-2", "Moby Dick", 635, authors.get(1)),
			new Book("book-3", "Interview with the vampire", 371, authors.get(1)));

	public static Author getAuthorById(String id) {
		return authors.stream().filter(author -> author.getId().equals(id)).findFirst().orElse(null);
	}

	public static Book getBookById(String id) {
		System.out.println("ici avec " + id);
		return books.stream().filter(book -> book.getId().equals(id)).findFirst().orElse(null);
	}

	public static Author getAuthorByLastName(String lastName) {
		System.out.print("ici");
		return authors.stream().filter(author -> author.getLastName().equals(lastName)).findFirst().orElse(null);
	}
}
