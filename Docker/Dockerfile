# Image for the 2EL5020 course

# user = abc, password = abc
# sudo: no password

# Build:
# docker build -t 2el5020 .

# Run:
# docker run --rm --detach --publish 3000:3000 --publish 3001:3001 --env TITLE=SOA 2el5020:latest
# or, if you want to "mount" a host directory to the /home/abc/PersistentDir directory in the container:
# docker run --rm --detach --publish 3000:3000 --publish 3001:3001 --env TITLE=SOA --volume YOUR_CHOICE_OF_PATH:/home/abc/PersistentDir:rw 2el5020:latest
# and to use your host user id and host group id (XXXX is given by id -u and YYYY by id -g):
# docker run --rm --detach --publish 3000:3000 --publish 3001:3001 --env TITLE=SOA --env PUID=XXXX --env PGID=YYYY --volume YOUR_CHOICE_OF_PATH:/home/abc/PersistentDir:rw 2el5020:latest

# Connect to:
# Use a web browser and visit http://127.0.0.1:3000/

#FROM	lscr.io/linuxserver/webtop:ubuntu-xfce 
# testée le 22 mai 2024, problème d'instabilité, retour à une ancienne version :
# attention, adpater les pip install en bas
#FROM	linuxserver/webtop:ubuntu-xfce-version-dbf4cbc9
FROM	linuxserver/webtop:ubuntu-xfce-version-9dbcf55c

# Give the abc user a regular home and nice prompt
RUN	mkdir /home/abc && mkdir /home/abc/Downloads && mkdir /home/abc/PersistentDir && chown -R abc:abc /home/abc && usermod -d /home/abc abc
RUN	echo "PS1='\u@graoully:\w\$ '" >> /home/abc/.bashrc
RUN 	echo "alias ls='ls --color=auto'" >> /etc/bash.bashrc
ENV	HOME=/home/abc

# Install a few utilities
RUN	apt-get update && apt-get upgrade -y && \
	apt-get install -y zip unzip git gedit tree iputils-ping wget vim net-tools traceroute evince

# get rid of gedit warning messages
RUN	echo "#!/bin/bash" > /usr/bin/gedit-null && echo "/usr/bin/gedit \""\$\@\"" 2>/dev/null" >> /usr/bin/gedit-null && chmod 755 /usr/bin/gedit-null
RUN	echo "alias gedit=/usr/bin/gedit-null" >> /etc/bash.bashrc 

# Install RESTer Firefox add-on 
RUN	wget https://addons.mozilla.org/firefox/downloads/file/3908154/rester-4.12.0-fx.xpi \
&&	mv rester-4.12.0-fx.xpi /usr/lib/firefox-addons/distribution/extensions/rester@kuehle.me.xpi 
# Install Altair GraphQL Client Firefox add-on
RUN	wget https://addons.mozilla.org/firefox/downloads/file/4244394/altair_graphql_client-6.3.1.xpi \
&&	mv altair_graphql_client-6.3.1.xpi /usr/lib/firefox-addons/distribution/extensions/{c336a627-bbea-4dbb-aa77-83899b52149a}.xpi

## Python
RUN	apt-get update && apt-get upgrade -y && \
	apt-get install -y python3 python3-venv python3-pip pip
# Pytest
RUN	apt-get install -y python3-pytest
# Python lib
# Pip install avec les options nécessaires avec la dernière version de l'image de base...
#RUN	pip3 install --no-cache-dir --upgrade --break-system-packages requests flask flask_cors apiflask graphql-core ariadne
RUN	pip install requests flask flask_cors apiflask graphql-core ariadne

# latest VSCodium version
RUN	wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
RUN	echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' | tee /etc/apt/sources.list.d/vscodium.list && apt update
RUN	apt install -y codium
RUN	echo "alias codium='codium --no-sandbox'" >> /etc/bash.bashrc
RUN	sed -i 's/\/usr\/share\/codium\/codium /\/usr\/share\/codium\/codium --no-sandbox /g' /usr/share/applications/codium.desktop 
# Python extension
RUN	mkdir -p /home/abc/.vscode-oss/extensions && chown -R abc:abc /home/abc/.vscode-oss && cd /home/abc/.vscode-oss/extensions && wget https://openvsxorg.blob.core.windows.net/resources/ms-python/python/2023.12.0/ms-python.python-2023.12.0.vsix && unzip ms-python.python-2023.12.0.vsix 

## Swagger Editor
RUN	wget https://github.com/swagger-api/swagger-editor/archive/refs/heads/master.zip \
&& 	unzip master.zip \
&& 	rm master.zip \
&& 	mv swagger-editor-master /home/abc/ 

# Clean up
RUN	apt-get clean && \
	rm -rf \
	/tmp/* \
	/var/lib/apt/lists/* \
	/var/tmp/*

